#include <iostream>
#include "Block.cpp"

using namespace std;

class Game {
private:
	int p1Points;
	int p2Points;
	BlockList* boardBlocks;

public:
	Game() {
		p1Points = 0;
		p2Points = 0;
	}

	~Game() {
		delete boardBlocks;
	}

	// Sets up the game board.
	void createBoard() {
		int i = 0;
		// Builds the list.
		boardBlocks = new BlockList;

		// Adds 27 blocks to list.
		for (; i < 27; i++) {
			boardBlocks->addBlock();
		}
	}

	void placeCherry() {
		Cherry* setBlock = new Cherry;
		char layer;
		int position;
		bool done = false;

		cout << "\nPlace your Wild Cherry Block";
		layer = inputLayer("two");
		position = inputPosition("two");

		boardBlocks->swapBlock(layer, position, setBlock);

		printBoard();
	}

	void placeCement() {
		Cement* setBlock = new Cement;
		char layer;
		int position;
		bool valid = false;

		cout << "\nPlace your Cement Block";
		layer = inputLayer("one");
		position = inputPosition("one");

		boardBlocks->swapBlock(layer, position, setBlock);

		printBoard();
	}

	char inputLayer(string currentPlayer) {
		string layer;
		bool valid = false;

		do {
			cout << "\nPlayer " << currentPlayer << " enter a layer (eg. a, b, c): ";
			getline(cin, layer); // getline takes care of spaces

			if (layer.compare("") == 0) {
				cout << "\nERROR: You must enter a layer.\n";
			}
			else if (layer.length() > 1) {
				cout << "\nERROR: The specified layer is invalid.\n";
			}
			else if (tolower(layer.at(0)) == 'a' || tolower(layer.at(0)) == 'b' || tolower(layer.at(0)) == 'c') {
				valid = true;
			}
			else {
				cout << "\nERROR: The layer you selected does not exist.\n";
			}
		} while (!valid);

		return tolower(layer.at(0));
	}

	int inputPosition(string currentPlayer) {
		string input;
		int position = 0;
		bool valid = false;

		do {
			cout << "Player " << currentPlayer << " enter a position (eg. 1-9): ";
			getline(cin, input);
			position = input.compare("") == 0 ? 0 : input.at(0) - '0';

			if (input.compare("") == 0) {
				cout << "\nERROR: You must enter a position.\n\n";
			}
			else if (input.length() > 1 || isdigit(position) || position < 1 || position > 9) {
				cout << "\nERROR: The position you entered is invalid.\n\n";
			}
			else {
				valid = true;
			}
			
		} while (!valid);

		return position;
	}

	void buildCube() {
		// so it begins, the great instantiation of our time.
		boardBlocks->buildCube();
	}

	// return boolean value to notify player of valid or invalid move.
	// changes value to display to printed board
	// calls setPiece in Block.cpp to change piece in list
	bool playMove(char layer, int position, int pieceValue) {
		bool valid = false;

		// if setting piece is completed successfully, valid = true. player must go again if false.
		if (boardBlocks->checkBlock(layer, position)) {
			cout << "\nERROR: A game piece already exists in this spot. Try again.\n";
		}
		else {
			boardBlocks->setPiece(layer, position, pieceValue);
			valid = true;
		}

		return valid;
	}

	// checks to see if the block placed scores a point or not.
	// checks two blocks out in the same direction for matching piece values.
	// checks opposing blocks for matching piece values.
	void scoreCount(char layer, int position, int pieceValue) {
		int checkPosition = 0;
		string currentBlockPiece;
		Block* currentBlock;
		bool isCherry = false; // set to true if any pointer is a wild cherry

		switch (layer) {
		case 'a': checkPosition = 0 + position; break;
		case 'b': checkPosition = 9 + position; break;
		case 'c': checkPosition = 18 + position; break;
		default: break;
		}

		currentBlockPiece = boardBlocks->getPiece(layer, position - 1);
		currentBlock = boardBlocks->iterate(checkPosition);

		// black hole.... of pointers.... i mean if statements.... ish....
		// Iterates through all of the pointers for the node at the entered position.
		for (int i = 0; i < 26; i++) {
			if (currentBlock->ptr[i] != NULL) {
				// Check if current = next piece or if next piece is cherry
				if (currentBlockPiece.compare(currentBlock->ptr[i]->piece) == 0 || currentBlock->ptr[i]->piece.compare("w") == 0) {
					if (currentBlock->ptr[i]->ptr[i] != NULL) {
						// Check if current = next piece after next or if next after next piece is cherry
						if (currentBlockPiece.compare(currentBlock->ptr[i]->ptr[i]->piece) == 0 || currentBlock->ptr[i]->ptr[i]->piece.compare("w") == 0) { // Checks two pointers out for similarities.
							if (currentBlock->ptr[i]->piece.compare("w") == 0 || currentBlock->ptr[i]->ptr[i]->piece.compare("w") == 0)
								isCherry = true;

							if (pieceValue == 1 && isCherry) p1Points += 2;
							else if (pieceValue == 1) p1Points++;
							else if (pieceValue == 2 && isCherry) p2Points += 2;
							else if (pieceValue == 2) p2Points++;
						}
					}
					if (i < 13 && currentBlock->ptr[i + 13] != NULL) {
						// Check if opposite piece is same as current or if opposite piece is cherry
						if (currentBlockPiece.compare(currentBlock->ptr[i + 13]->piece) == 0 || currentBlock->ptr[i + 13]->piece.compare("w") == 0) { // Checks opposite of pointer for similarities.
							if (currentBlock->ptr[i]->piece.compare("w") == 0 || currentBlock->ptr[i + 13]->piece.compare("w") == 0)
								isCherry = true;
							
							if (pieceValue == 1 && isCherry) p1Points += 2;
							else if (pieceValue == 1) p1Points++;
							else if (pieceValue == 2 && isCherry) p2Points += 2;
							else if (pieceValue == 2) p2Points++;
						}
					}
				}
			}
		}
	}

	int getP1Points() {
		return p1Points;
	}

	int getP2Points() {
		return p2Points;
	}

	// printBoard() prints player scores as well as placed game pieces
	void printBoard() {
		cout << "________________________________________________________________\n"
			<< "\n\t\t\t3D Tic Tac Toe\n\n"
			<< "\t ____ A ____      ____ B ____      ____ C ____    \n"
			<< "\t|   |   |   |    |   |   |   |    |   |   |   |   \n"
			<< "\t| " << boardBlocks->getPiece('a', 0) << " | " << boardBlocks->getPiece('a', 1) << " | " << boardBlocks->getPiece('a', 2) << " |    | " << boardBlocks->getPiece('b', 0) << " | " << boardBlocks->getPiece('b', 1) << " | " << boardBlocks->getPiece('b', 2) << " |    | " << boardBlocks->getPiece('c', 0) << " | " << boardBlocks->getPiece('c', 1) << " | " << boardBlocks->getPiece('c', 2) << " | \n"
			<< "\t|___|___|___|    |___|___|___|    |___|___|___|   \n"
			<< "\t|   |   |   |    |   |   |   |    |   |   |   |   \n"
			<< "\t| " << boardBlocks->getPiece('a', 3) << " | " << boardBlocks->getPiece('a', 4) << " | " << boardBlocks->getPiece('a', 5) << " |    | " << boardBlocks->getPiece('b', 3) << " | " << boardBlocks->getPiece('b', 4) << " | " << boardBlocks->getPiece('b', 5) << " |    | " << boardBlocks->getPiece('c', 3) << " | " << boardBlocks->getPiece('c', 4) << " | " << boardBlocks->getPiece('c', 5) << " | \n"
			<< "\t|___|___|___|    |___|___|___|    |___|___|___|   \n"
			<< "\t|   |   |   |    |   |   |   |    |   |   |   |   \n"
			<< "\t| " << boardBlocks->getPiece('a', 6) << " | " << boardBlocks->getPiece('a', 7) << " | " << boardBlocks->getPiece('a', 8) << " |    | " << boardBlocks->getPiece('b', 6) << " | " << boardBlocks->getPiece('b', 7) << " | " << boardBlocks->getPiece('b', 8) << " |    | " << boardBlocks->getPiece('c', 6) << " | " << boardBlocks->getPiece('c', 7) << " | " << boardBlocks->getPiece('c', 8) << " | \n"
			<< "\t|___|___|___|    |___|___|___|    |___|___|___|   \n"
			<< "                                                    \n"
			<< "\tPlayer 1: " << this->p1Points << " points          Player 2: " << this->p2Points << " points    \n"
			<< "________________________________________________________________\n";
	}
};