#include <iostream>
#include <string>

using namespace std;

class Block {
protected:
	int ptrCount;

public:
	string piece;
	int index;
	Block *next;
	Block *ptr[26]; // The great initialization of our time lies ahead.

	Block() {
		piece = '1'; 
		next = NULL;
	}
};

class Cherry : public Block {
public:
	Cherry() {
		piece = 'w';
		next = NULL;
	}
};

class Cement : public Block {
public:
	Cement() {
		piece = 'c';
		next = NULL;
	}
};

class BlockList {
private:
	Block *head = NULL;
	Block *tail;
	int length = 0;

public:
	~BlockList() {
		Block* deleteNode;
		for (deleteNode = head; head; deleteNode = head) {
			head = head->next;
			delete deleteNode;
		}
	}

	// Checks if the list is empty. Returns true for an empty list.
	bool isEmpty() {
		return (head == NULL) ? (true) : (false);
	}

	// Iterates to position and returns a pointer to the block of desired position.
	Block* iterate(int position) {
		Block* iterator = head;
		int i;

		for (i = 1; i < position; i++) {
			iterator = (iterator->next != NULL) ? iterator->next : iterator;
		}

		return iterator;
	}

	// Adds a block to the end of the list.
	void addBlock() {
		int testValue = 0;
		if (isEmpty()) {  // If the list is empty create a new block and have head point to it.
			Block* newBlock = new Block;
			length = 0;
			head = newBlock;
			tail = head;

			tail->index = length++; // assigns index to head block
			tail->piece = to_string(length);
		}
		else {  // If the list is not empty iterate to the end of the list and add a node.
			Block* newBlock = new Block;
			tail->next = newBlock;
			tail = tail->next;
			tail->index = length++; // assigns index to each block after head.

			// Initializes pieces to display position value. (1-9 per layer)
			if (length <= 9)
				tail->piece = to_string(length);
			else if (length > 9 && length <= 18)
				tail->piece = to_string(length - 9);
			else
				tail->piece = to_string(length - 18);
		}
	}

	// Checks to see if something already exists at the indicated position.
	// Returns false if nothing found, true if something found.
	bool checkBlock(char layer, int position) {
		Block* iterator = switchLayer(layer);

		for (int i = 1; i <= position; i++) {
			if (i == position) {
				return (iterator->piece.compare("x") == 0
					|| iterator->piece.compare("o") == 0
					|| iterator->piece.compare("c") == 0
					|| iterator->piece.compare("w") == 0) ? true : false;
			}
			iterator = iterator->next;
		}
	}

	// Assigns the corresponding piece at the indicated block.
	// Returns true if succeeded, false if failed.
	bool setPiece(char layer, int position, int pieceValue) {
		Block* iterator = NULL;
		string gamePiece;

		// pieceValue sets to corresponding gamePiece (1 = x, 2 = o, 3 = cherry, 4 = cement)
		switch (pieceValue) {
		case 1: gamePiece = "x"; break;
		case 2: gamePiece = "o"; break;
		case 3: gamePiece = "w"; break;
		case 4: gamePiece = "c"; break;
		default: return false;
			break;
		}

		// When layer ___, increment to beginning of that layer then increment to position and set.
		iterator = switchLayer(layer);

		for (int i = 1; i <= position; i++) {
			// If statement checks for correct position to set piece & for existing piece.
			if (i == position && !checkBlock(layer, position)) {
				iterator->piece = gamePiece;  // Assigns the piece at the appropriate location in the list.
				return true;
			}
			iterator = iterator->next;
		}

		return false;
	}

	// Returns the value of piece at the specified position.
	string getPiece(char layer, int position) {
		Block* iterator = switchLayer(layer);

		for (int i = 0; i <= position; i++) {
			if (i == position) {
				return iterator->piece;
			}
			iterator = iterator->next;
		}
	}

	Block* switchLayer(char layer) {
		Block* iterator = NULL;

		switch (layer) {
		case 'a': iterator = head; break;
		case 'b': iterator = iterate(10); break;
		case 'c': iterator = iterate(19); break;
		default:
			break;
		}

		return iterator;
	}

	void swapBlock(char layer, int position, Block* setBlock) {
		Block* iterator = switchLayer(layer);
		Block* dropBlock = NULL;

		if (position == 1) {
			switch (layer) {
			case 'a':
				dropBlock = iterator;
				setBlock->next = iterator->next;
				head = setBlock;
				break;
			case 'b':
				iterator = switchLayer('a');
				for (int i = 1; i < 9; i++)
					iterator = iterator->next;
				break;
			case 'c':
				iterator = switchLayer('b');
				for (int i = 1; i < 9; i++)
					iterator = iterator->next;
				break;
			default:
				break;
			}
		} else {
			for (int i = 1; i < position - 1; i++)
				iterator = iterator->next;
		}

		if (!(position == 1 && layer == 'a')) {
			dropBlock = iterator->next;
			setBlock->next = iterator->next->next;
			iterator->next = setBlock;
		}

		delete(dropBlock);
	}

	void buildCube() {									// vv Pain and agony lie below. vv
		for (int i = 1; i <= length; i++) {
			Block* setBlock = iterate(i);
			switch (i) {								// Oh god why.
			case 1:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 25: setBlock->ptr[j] = iterate(2); break;
					case 23: setBlock->ptr[j] = iterate(4); break;
					case 22: setBlock->ptr[j] = iterate(5); break;
					case 17: setBlock->ptr[j] = iterate(10); break;
					case 16: setBlock->ptr[j] = iterate(11); break;
					case 14: setBlock->ptr[j] = iterate(13); break;
					case 13: setBlock->ptr[j] = iterate(14); break;
					default: break;
					}
				}
				break;
			case 2:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 12: setBlock->ptr[j] = iterate(1); break;
					case 25: setBlock->ptr[j] = iterate(3); break;
					case 23: setBlock->ptr[j] = iterate(5); break;
					case 17: setBlock->ptr[j] = iterate(11); break;
					case 14: setBlock->ptr[j] = iterate(14); break;
					default: break;
					}
				}
				break;
			case 3:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 12: setBlock->ptr[j] = iterate(2); break;
					case 24: setBlock->ptr[j] = iterate(5); break;
					case 23: setBlock->ptr[j] = iterate(6); break;
					case 18: setBlock->ptr[j] = iterate(11); break;
					case 17: setBlock->ptr[j] = iterate(12); break;
					case 15: setBlock->ptr[j] = iterate(14); break;
					case 14: setBlock->ptr[j] = iterate(15); break;
					default: break;
					}
				}
				break;
			case 4:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 10: setBlock->ptr[j] = iterate(1); break;
					case 25: setBlock->ptr[j] = iterate(5); break;
					case 23: setBlock->ptr[j] = iterate(7); break;
					case 17: setBlock->ptr[j] = iterate(13); break;
					case 16: setBlock->ptr[j] = iterate(14); break;
					default: break;
					}
				}
				break;
			case 5:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 9: setBlock->ptr[j] = iterate(1); break;
					case 10: setBlock->ptr[j] = iterate(2); break;
					case 11: setBlock->ptr[j] = iterate(3); break;
					case 12: setBlock->ptr[j] = iterate(4); break;
					case 25: setBlock->ptr[j] = iterate(6); break;
					case 24: setBlock->ptr[j] = iterate(7); break;
					case 23: setBlock->ptr[j] = iterate(8); break;
					case 22: setBlock->ptr[j] = iterate(9); break;
					case 17: setBlock->ptr[j] = iterate(14); break;
					default: break;
					}
				}
				break;
			case 6:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 10: setBlock->ptr[j] = iterate(3); break;
					case 12: setBlock->ptr[j] = iterate(5); break;
					case 23: setBlock->ptr[j] = iterate(9); break;
					case 18: setBlock->ptr[j] = iterate(14); break;
					case 17: setBlock->ptr[j] = iterate(15); break;
					default: break;
					}
				}
				break;
			case 7:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 10: setBlock->ptr[j] = iterate(4); break;
					case 11: setBlock->ptr[j] = iterate(5); break;
					case 25: setBlock->ptr[j] = iterate(8); break;
					case 20: setBlock->ptr[j] = iterate(13); break;
					case 19: setBlock->ptr[j] = iterate(14); break;
					case 17: setBlock->ptr[j] = iterate(16); break;
					case 16: setBlock->ptr[j] = iterate(17); break;
					default: break;
					}
				}
				break;
			case 8:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 10: setBlock->ptr[j] = iterate(5); break;
					case 12: setBlock->ptr[j] = iterate(7); break;
					case 25: setBlock->ptr[j] = iterate(9); break;
					case 20: setBlock->ptr[j] = iterate(14); break;
					case 17: setBlock->ptr[j] = iterate(17); break;
					default: break;
					}
				}
				break;
			case 9:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 9: setBlock->ptr[j] = iterate(5); break;
					case 10: setBlock->ptr[j] = iterate(6); break;
					case 12: setBlock->ptr[j] = iterate(8); break;
					case 21: setBlock->ptr[j] = iterate(14); break;
					case 20: setBlock->ptr[j] = iterate(15); break;
					case 18: setBlock->ptr[j] = iterate(17); break;
					case 17: setBlock->ptr[j] = iterate(18); break;
					default: break;
					}
				}
				break;
			case 10:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(1); break;
					case 25: setBlock->ptr[j] = iterate(11); break;
					case 23: setBlock->ptr[j] = iterate(13); break;
					case 22: setBlock->ptr[j] = iterate(14); break;
					case 17: setBlock->ptr[j] = iterate(19); break;
					default: break;
					}
				}
				break;
			case 11:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 3: setBlock->ptr[j] = iterate(1); break;
					case 4: setBlock->ptr[j] = iterate(2); break;
					case 5: setBlock->ptr[j] = iterate(3); break;
					case 12: setBlock->ptr[j] = iterate(10); break;
					case 25: setBlock->ptr[j] = iterate(12); break;
					case 23: setBlock->ptr[j] = iterate(14); break;
					case 18: setBlock->ptr[j] = iterate(19); break;
					case 17: setBlock->ptr[j] = iterate(20); break;
					case 16: setBlock->ptr[j] = iterate(21); break;
					default: break;
					}
				}
				break;
			case 12:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(3); break;
					case 12: setBlock->ptr[j] = iterate(11); break;
					case 24: setBlock->ptr[j] = iterate(14); break;
					case 23: setBlock->ptr[j] = iterate(15); break;
					case 17: setBlock->ptr[j] = iterate(21); break;
					default: break;
					}
				}
				break;
			case 13:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 1: setBlock->ptr[j] = iterate(1); break;
					case 4: setBlock->ptr[j] = iterate(4); break;
					case 7: setBlock->ptr[j] = iterate(7); break;
					case 10: setBlock->ptr[j] = iterate(10); break;
					case 25: setBlock->ptr[j] = iterate(14); break;
					case 23: setBlock->ptr[j] = iterate(16); break;
					case 20: setBlock->ptr[j] = iterate(19); break;
					case 17: setBlock->ptr[j] = iterate(22); break;
					case 14: setBlock->ptr[j] = iterate(25); break;
					default: break;
					}
				}
				break;
			case 14:  // Big daddy. Mother of all Pointers
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 0: setBlock->ptr[j] = iterate(1); break;
					case 1: setBlock->ptr[j] = iterate(2); break;
					case 2: setBlock->ptr[j] = iterate(3); break;
					case 3: setBlock->ptr[j] = iterate(4); break;
					case 4: setBlock->ptr[j] = iterate(5); break;
					case 5: setBlock->ptr[j] = iterate(6); break;
					case 6: setBlock->ptr[j] = iterate(7); break;
					case 7: setBlock->ptr[j] = iterate(8); break;
					case 8: setBlock->ptr[j] = iterate(9); break;
					case 9: setBlock->ptr[j] = iterate(10); break;
					case 10: setBlock->ptr[j] = iterate(11); break;
					case 11: setBlock->ptr[j] = iterate(12); break;
					case 12: setBlock->ptr[j] = iterate(13); break;
					case 25: setBlock->ptr[j] = iterate(15); break;
					case 24: setBlock->ptr[j] = iterate(16); break;
					case 23: setBlock->ptr[j] = iterate(17); break;
					case 22: setBlock->ptr[j] = iterate(18); break;
					case 21: setBlock->ptr[j] = iterate(19); break;
					case 20: setBlock->ptr[j] = iterate(20); break;
					case 19: setBlock->ptr[j] = iterate(21); break;
					case 18: setBlock->ptr[j] = iterate(22); break;
					case 17: setBlock->ptr[j] = iterate(23); break;
					case 16: setBlock->ptr[j] = iterate(24); break;
					case 15: setBlock->ptr[j] = iterate(25); break;
					case 14: setBlock->ptr[j] = iterate(26); break;
					case 13: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 15:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 1: setBlock->ptr[j] = iterate(3); break;
					case 4: setBlock->ptr[j] = iterate(6); break;
					case 7: setBlock->ptr[j] = iterate(9); break;
					case 10: setBlock->ptr[j] = iterate(12); break;
					case 12: setBlock->ptr[j] = iterate(14); break;
					case 23: setBlock->ptr[j] = iterate(18); break;
					case 20: setBlock->ptr[j] = iterate(21); break;
					case 17: setBlock->ptr[j] = iterate(24); break;
					case 14: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 16:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(7); break;
					case 10: setBlock->ptr[j] = iterate(13); break;
					case 11: setBlock->ptr[j] = iterate(14); break;
					case 25: setBlock->ptr[j] = iterate(17); break;
					case 17: setBlock->ptr[j] = iterate(25); break;
					default: break;
					}
				}
				break;
			case 17:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 3: setBlock->ptr[j] = iterate(7); break;
					case 4: setBlock->ptr[j] = iterate(8); break;
					case 5: setBlock->ptr[j] = iterate(9); break;
					case 10: setBlock->ptr[j] = iterate(14); break;
					case 12: setBlock->ptr[j] = iterate(16); break;
					case 25: setBlock->ptr[j] = iterate(18); break;
					case 18: setBlock->ptr[j] = iterate(25); break;
					case 17: setBlock->ptr[j] = iterate(26); break;
					case 16: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 18:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(9); break;
					case 9: setBlock->ptr[j] = iterate(14); break;
					case 10: setBlock->ptr[j] = iterate(15); break;
					case 12: setBlock->ptr[j] = iterate(17); break;
					case 17: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 19:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(10); break;
					case 5: setBlock->ptr[j] = iterate(11); break;
					case 7: setBlock->ptr[j] = iterate(13); break;
					case 8: setBlock->ptr[j] = iterate(14); break;
					case 22: setBlock->ptr[j] = iterate(23); break;
					case 23: setBlock->ptr[j] = iterate(22); break;
					case 25: setBlock->ptr[j] = iterate(20); break;
					default: break;
					}
				}
				break;
			case 20:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(11); break;
					case 7: setBlock->ptr[j] = iterate(14); break;
					case 12: setBlock->ptr[j] = iterate(19); break;
					case 23: setBlock->ptr[j] = iterate(23); break;
					case 25: setBlock->ptr[j] = iterate(21); break;
					default: break;
					}
				}
				break;
			case 21:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 3: setBlock->ptr[j] = iterate(11); break;
					case 4: setBlock->ptr[j] = iterate(12); break;
					case 6: setBlock->ptr[j] = iterate(14); break;
					case 7: setBlock->ptr[j] = iterate(15); break;
					case 12: setBlock->ptr[j] = iterate(20); break;
					case 23: setBlock->ptr[j] = iterate(24); break;
					case 24: setBlock->ptr[j] = iterate(23); break;
					default: break;
					}
				}
				break;
			case 22:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(13); break;
					case 5: setBlock->ptr[j] = iterate(14); break;
					case 10: setBlock->ptr[j] = iterate(19); break;
					case 23: setBlock->ptr[j] = iterate(25); break;
					case 25: setBlock->ptr[j] = iterate(23); break;
					default: break;
					}
				}
				break;
			case 23:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 4: setBlock->ptr[j] = iterate(14); break;
					case 9: setBlock->ptr[j] = iterate(19); break;
					case 10: setBlock->ptr[j] = iterate(20); break;
					case 11: setBlock->ptr[j] = iterate(21); break;
					case 12: setBlock->ptr[j] = iterate(22); break;
					case 22: setBlock->ptr[j] = iterate(27); break;
					case 23: setBlock->ptr[j] = iterate(26); break;
					case 24: setBlock->ptr[j] = iterate(25); break;
					case 25: setBlock->ptr[j] = iterate(24); break;
					default: break;
					}
				}
				break;
			case 24:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 3: setBlock->ptr[j] = iterate(14); break;
					case 4: setBlock->ptr[j] = iterate(15); break;
					case 10: setBlock->ptr[j] = iterate(21); break;
					case 12: setBlock->ptr[j] = iterate(23); break;
					case 23: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 25:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 1: setBlock->ptr[j] = iterate(13); break;
					case 2: setBlock->ptr[j] = iterate(14); break;
					case 4: setBlock->ptr[j] = iterate(16); break;
					case 5: setBlock->ptr[j] = iterate(17); break;
					case 10: setBlock->ptr[j] = iterate(22); break;
					case 11: setBlock->ptr[j] = iterate(23); break;
					case 25: setBlock->ptr[j] = iterate(26); break;
					default: break;
					}
				}
				break;
			case 26:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 1: setBlock->ptr[j] = iterate(14); break;
					case 4: setBlock->ptr[j] = iterate(17); break;
					case 10: setBlock->ptr[j] = iterate(23); break;
					case 12: setBlock->ptr[j] = iterate(25); break;
					case 25: setBlock->ptr[j] = iterate(27); break;
					default: break;
					}
				}
				break;
			case 27:
				for (int j = 0; j < 26; j++) {
					switch (j) {
					case 0: setBlock->ptr[j] = iterate(14); break;
					case 1: setBlock->ptr[j] = iterate(15); break;
					case 3: setBlock->ptr[j] = iterate(17); break;
					case 4: setBlock->ptr[j] = iterate(18); break;
					case 9: setBlock->ptr[j] = iterate(23); break;
					case 10: setBlock->ptr[j] = iterate(24); break;
					case 12: setBlock->ptr[j] = iterate(26); break;
					default: break;
					}
				}
				break;
			default:
				break;
			}
		}
	}
};