#include <iostream>
#include <string>
#include <cctype>
#include "Game.cpp"

using namespace std;

void main() {
	Game* myGame;
	int turn, position, pieceValue = 1;			// pieceValue: '1' when player one, '2' when player two
	bool quit = false, validMove = false;
	char rematch, layer;
	string currentPlayer = "one";

	do {
		myGame = new Game;

		myGame->createBoard();					//Set up list of blocks then print initial board
		myGame->printBoard();

		myGame->placeCement();					// Places wild cherry and cement block
		myGame->placeCherry();

		myGame->buildCube();					// Set up pointers for scoring system

		// 25 blocks = 25 turns (after cherry/cement)
		for (turn = 1; turn <= 25; turn++) {
			position = 0;

			// determine player's turn based on odd/even
			if (turn % 2 == 0) {
				currentPlayer = "two";
				pieceValue = 2;
			} else {
				currentPlayer = "one";
				pieceValue = 1;
			}
			
			do {
				layer = myGame->inputLayer(currentPlayer);
				position = myGame->inputPosition(currentPlayer);
				validMove = myGame->playMove(layer, position, pieceValue);
			} while (!validMove);
			
			myGame->scoreCount(layer, position, pieceValue);
			myGame->printBoard();				// Print latest updated board
		}

		// Finalize scores and display who wins
		if (myGame->getP1Points() == myGame->getP2Points()) {
			cout << "\nIt's a draw! Everybody wins!\n";
		} else if (myGame->getP1Points() > myGame->getP2Points()) {
			cout << "\nCongratulations, Player One! You won!\n";
		} else {
			cout << "\nCongratulations, Player Two! You won!\n";
		}

		delete myGame;

		cout << "\nWould you like a rematch? (y/n): ";
		cin >> rematch;

		quit = (rematch == 'y') ? false : true;	// prompt players for rematch, then change this value
	} while (!quit);
}